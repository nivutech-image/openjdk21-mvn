FROM openjdk:21-ea-32-jdk-bullseye

# Instalación de Maven
RUN apt-get update && \
    apt-get install -y maven curl && \
    rm -rf /var/lib/apt/lists/*

# Verificar las instalaciones
RUN java -version && mvn -version;